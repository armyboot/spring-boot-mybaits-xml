package com.army;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.army.mapper")
public class SpringBootMybaitsXmlApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMybaitsXmlApplication.class, args);
    }
}
