package com.army.mapper;

import com.army.entity.UserEntity;
import com.army.param.UserParam;

import java.util.List;

public interface UserMapper {
    void insert(UserEntity user);
    int update(UserEntity user);
    int delete(Long id);

    List<UserEntity> getAll();
    List<UserEntity> getList(UserParam userParm);
    int getCount(UserParam userParam);
    UserEntity getOne(Long id);
}
